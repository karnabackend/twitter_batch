from pymongo import MongoClient
import datetime
import logging
import re
import json
from collections import Counter
import time
import os
import collections
import pandas as pd
import traceback
import time
import MySQLdb
import MySQLdb as db
import MySQLdb.cursors
from bson.objectid import ObjectId
from twitter_config import *
import math
import gc
import parmap

mongo_dev_url = os.environ['MONGO_DEV_URL']
mongo_prod_url=os.environ["MONGO_PROD_URL"]
mongo_url = os.environ["MONGOURL"]
logging.basicConfig(filename='/home/ankit/twitter_batch/tweets_graph.log',level=logging.INFO)


def get_sql_connection():
	connection= db.connect(host='memsql.paralleldots.com',user='ankit',passwd='mongodude123',db="socialmedia")     
	return connection


def get_sql_data(sql_data):
	conn=get_sql_connection()
	conn.set_character_set('utf8')
	cursorobj=conn.cursor()
	cursorobj.execute("Select id,alert_status,tw_track_start_time,modified_time,twitter_status from alerts where twitter=1 order by id desc")
	for row in cursorobj.fetchall():
		key=row[0]
		status=row[1]
		track_time=row[2]
		modified_time=row[3]
		twitter_status=row[4]
	
		
		current_time=datetime.datetime.now()
		if modified_time is not None:
			diff_time=(current_time-modified_time).total_seconds()/3600
		if (status=='active' and twitter_status=='active') or (diff_time<0.25):
			sql_data.append({"alert":key,"track_time":track_time})

	conn.close()
	return sql_data



def get_diff_data(FREQ,diff,posts,sentiment,tweets_count,users_count,tweets_per_hour):
	if diff<FREQ:
		diff_tweets=''
		diff_users=''
		diff_tweets_hour=''
		old_senti='neutral'
		new_senti=sentiment

	else:

		if posts['topstats']['tweets_count']!=0:
			diff_tweets=(posts['topstats']['tweets_count']-tweets_count)*100/posts['topstats']['tweets_count']
		elif tweets_count==0:
			diff_tweets=0
		else:
			diff_tweets=int((0.01-tweets_count)*100/0.01)

		if posts['topstats']['users_count']!=0:
			diff_users=(posts['topstats']['users_count']-users_count)*100/posts['topstats']['users_count']
		elif users_count==0:
			diff_users=0
		else:
			diff_users=int((0.01-users_count)*100/0.01)


		if posts['topstats']['tweets_per_hour']!=0:
			diff_tweets_hour=(posts['topstats']['tweets_per_hour']-tweets_per_hour)*100/posts['topstats']['tweets_per_hour']
		elif tweets_per_hour==0:
			diff_tweets_hour=0
		else:
			diff_tweets_hour=int((0.01-tweets_per_hour)*100/0.01)
		old_senti=posts['topstats']['sentiment']
		new_senti=sentiment

	return diff_tweets,diff_users,diff_tweets_hour,old_senti,new_senti


def get_daily_data(alert_info,enddate,FREQ,client):
	key=alert_info['alert']
	track_time=alert_info['track_time']

	tweets_db = MongoClient(mongo_url,27017)["GnipDataFinal"]["tweets"]
	column=MongoClient(mongo_url,27017)["dev_twitter"][client]
	
	start_date=pd.to_datetime((enddate-datetime.timedelta(days=FREQ-1)).date())
	output={}
	data = tweets_db.find({"alertid":{"$in":[key]},"tweet.timestamp":{"$gte":start_date,"$lte":enddate}}).sort("_id",-1)
	
	tweets_data=[x for x in data]
	tweets_analysis=pd.DataFrame()
	tweets_analysis['timestamp']=map(lambda tweet: tweet['tweet']['timestamp'] if tweet['tweet'].get('timestamp', None) != None else None, tweets_data)
	tweets_analysis['sentiment']=map(lambda tweet: tweet['sentiment'] if tweet['sentiment'] != None else None, tweets_data)
	tweets_analysis['userID']=map(lambda tweet: tweet['tweet']['user']['id'] if tweet['tweet']['user']['id'] != None else None, tweets_data)
	tweets_analysis['language']=map(lambda tweet: tweet['tweet']['language'] if tweet['tweet']['language'] != None else None, tweets_data)

	timestamp_count=[]
	if tweets_analysis.empty==False:


		tweets_count=tweets_analysis['timestamp'].count()
		diff_days=((tweets_analysis['timestamp'].max()).date()-track_time.date()).days+1
		diff=[FREQ if diff_days>=FREQ else diff_days][0]
		tweets_per_day=int(tweets_count/diff)

		users_count=len(tweets_analysis['userID'].unique())
		pos_graph=tweets_analysis[(tweets_analysis['sentiment']>=0.7)]
		neg_graph=tweets_analysis[(tweets_analysis['sentiment']<=0.3)]
		neutral_graph=tweets_analysis[(tweets_analysis['sentiment']>0.3) & (tweets_analysis['sentiment']<0.7)]


		overall=[pos_graph['sentiment'].count(),neg_graph['sentiment'].count(),neutral_graph['sentiment'].count()]
		sentiment=['positive','negative','neutral'][overall.index(max(overall))]
		en_graph=tweets_analysis[(tweets_analysis['language']=='en')]
		df1 = pos_graph.set_index('timestamp')
		df2 = neg_graph.set_index('timestamp')
		df3 = en_graph.set_index('timestamp')
		df4 = tweets_analysis.set_index('timestamp')

		df1['pos_count']=1
		df2['neg_count']=1
		df3['en_count']=1
		df4['count']=1
		pos_counts = df1.groupby([pd.Grouper(freq='1D',label='left',closed='left')],axis=0)['pos_count'].count()
		neg_counts = df2.groupby([pd.Grouper(freq='1D',label='left',closed='left')],axis=0)['neg_count'].count()
		en_counts = df3.groupby([pd.Grouper(freq='1D',label='left',closed='left')],axis=0)['en_count'].count()
		counts = df4.groupby([pd.Grouper(freq='1D',label='left',closed='left')],axis=0)['count'].count()
		df_new=pd.concat([pos_counts,neg_counts,en_counts,counts],axis=1)
		df_new['others']=df_new['count']-df_new['en_count']
		# print tweets_count,df_new

	else:
		df_new=pd.DataFrame()
		df_new['pos_count']=0
		df_new['neg_count']=0
		df_new['en_count']=0
		df_new['count']=0
		df_new['others']=0
		df_new.index = pd.to_datetime(df_new.index)
		tweets_count=0
		users_count=0
		tweets_per_day=0
		sentiment='neutral'
		diff_days=((enddate.date()-track_time.date())).days+1


	rng=pd.date_range(start_date,enddate)
	df_new.index=pd.DatetimeIndex(df_new.index)
	test=df_new.reindex(rng,fill_value=0).fillna(0)
	test['date']=test.index.strftime("%d-%b")

	test['year']=test.index.year
	test['year']=test['year'].apply(str)
	output['topstats']={"tweets_count":tweets_count,"users_count":users_count,"tweets_per_hour":tweets_per_day,"sentiment":sentiment}
	output['timestamp_count']=eval(test.to_json(orient='records'))

	output['alertid']=key
	output['bucket']=str(FREQ)+'days'

	
	posts=column.find_one({"alertid":key})



	if posts is not None:
		diff_data=get_diff_data(FREQ,diff_days,posts,sentiment,tweets_count,users_count,tweets_per_day)
		diff_tweets=diff_data[0]
		diff_users=diff_data[1]
		diff_tweets_hour=diff_data[2]
		old_senti=diff_data[3]
		new_senti=diff_data[4]
		output['diff']={"diff_tweets":diff_tweets,"diff_users":diff_users,"diff_tweets_hour":diff_tweets_hour,"diff_senti":{"old_senti":old_senti,"new_senti":new_senti}}
		output['topstats']['last_updated']=str(datetime.datetime.now())
		column.update({'_id': ObjectId(str(posts['_id']))},{'$set':output})
	
	
	else:
		output['topstats']['last_updated']=str(datetime.datetime.now())
		output["diff"]={"diff_tweets":0,"diff_users":0,"diff_tweets_hour":0,"diff_senti":{"old_senti":0,"new_senti":0}}
		column.insert(output)

	
	# if os.environ['MONGOURL']=='mongodev.paralleldots.com':
	#     redis_key='dev_tw_'+str(user)+'_'+str(key)+'_'+str(output['bucket'])
	# else:
	#     redis_key='prod_tw_'+str(user)+'_'+str(key)+'_'+str(output['bucket'])
	# redis_output={}
	logging.info("%s days bucket for alert=%s at time %s",FREQ,key,str(datetime.datetime.now()))
	# redis_output['tweet']={"bucket":str(output['bucket']),"topstats":output['topstats'],"diff":output['diff'],"timestamp_count":output['timestamp']}
	# red.set(redis_key,redis_output)
			
def get_hourly_data(alert_info,enddate,FREQ,client):
	key=alert_info['alert']
	track_time=alert_info['track_time']
	
	tweets_db = MongoClient(mongo_url,27017)["GnipDataFinal"]["tweets"]
	column=MongoClient(mongo_url,27017)["dev_twitter"][client]
	
	start_date=pd.to_datetime((enddate-datetime.timedelta(hours=FREQ)))
	start_date=datetime.datetime.strptime(str(start_date.replace(minute=0,second=0)).split('.')[0],"%Y-%m-%d %H:%M:%S")


	output={}
	data = tweets_db.find({"alertid":{"$in":[key]},"tweet.timestamp":{"$gte":start_date+datetime.timedelta(hours=1),"$lte":enddate}}).sort("_id",-1)
	
	tweets_data=[x for x in data]
	tweets_analysis=pd.DataFrame()
	tweets_analysis['timestamp']=map(lambda tweet: tweet['tweet']['timestamp'] if tweet['tweet'].get('timestamp', None) != None else None, tweets_data)
	tweets_analysis['sentiment']=map(lambda tweet: tweet['sentiment'] if tweet['sentiment'] != None else None, tweets_data)
	tweets_analysis['userID']=map(lambda tweet: tweet['tweet']['user']['id'] if tweet['tweet']['user']['id'] != None else None, tweets_data)
	tweets_analysis['language']=map(lambda tweet: tweet['tweet']['language'] if tweet['tweet']['language'] != None else None, tweets_data)

	timestamp_count=[]
	if tweets_analysis.empty==False:
		tweets_count=tweets_analysis['timestamp'].count()
		diff_days=(tweets_analysis['timestamp'].max()-track_time).total_seconds()/float(3600)
		
		if FREQ==1:
			resample_freq='10Min'
			period=7
			diff=[FREQ if diff_days>=FREQ else diff_days][0]
			
		else:
			resample_freq='1H'
			period=FREQ+1
			diff=[FREQ if diff_days>=FREQ else diff_days][0]
			
		
		# start_date=datetime.datetime.strptime(str(start_date.replace(minute=0,second=0)).split('.')[0],"%Y-%m-%d %H:%M:%S")	
		tweets_per_hour=int(tweets_count/diff)

		users_count=len(tweets_analysis['userID'].unique())
		# print tweets_count,users_count,tweets_per_hour
		# tweets_per_day=tweets_analysis['timestamp'].mean()
		pos_graph=tweets_analysis[(tweets_analysis['sentiment']>=0.7)]
		neg_graph=tweets_analysis[(tweets_analysis['sentiment']<=0.3)]
		neutral_graph=tweets_analysis[(tweets_analysis['sentiment']>0.3) & (tweets_analysis['sentiment']<0.7)]
		

		overall=[pos_graph['sentiment'].count(),neg_graph['sentiment'].count(),neutral_graph['sentiment'].count()]
		sentiment=['positive','negative','neutral'][overall.index(max(overall))]
		en_graph=tweets_analysis[(tweets_analysis['language']=='en')]
		df1 = pos_graph.set_index('timestamp')
		df2 = neg_graph.set_index('timestamp')
		df3 = en_graph.set_index('timestamp')
		df4 = tweets_analysis.set_index('timestamp')
		
		df1['pos_count']=1
		df2['neg_count']=1
		df3['en_count']=1
		df4['count']=1

		pos_counts = df1.groupby([pd.Grouper(freq=resample_freq,label='right',closed='left')],axis=0)['pos_count'].count()
		neg_counts = df2.groupby([pd.Grouper(freq=resample_freq,label='right',closed='left')],axis=0)['neg_count'].count()
		en_counts = df3.groupby([pd.Grouper(freq=resample_freq,label='right',closed='left')],axis=0)['en_count'].count()
		counts = df4.groupby([pd.Grouper(freq=resample_freq,label='right',closed='left')],axis=0)['count'].count()
		df_new=pd.concat([pos_counts,neg_counts,en_counts,counts],axis=1)
		df_new['others']=df_new['count']-df_new['en_count']

	else:
		df_new=pd.DataFrame()
		df_new['pos_count']=0
		df_new['neg_count']=0
		df_new['en_count']=0
		df_new['count']=0
		df_new['others']=0
		df_new.index = pd.to_datetime(df_new.index)
		tweets_count=0
		users_count=0
		tweets_per_hour=0
		sentiment='neutral'
		diff_days=(enddate-track_time).total_seconds()/3600
		if FREQ==1:
			resample_freq='10Min'
			period=7
		else:
			resample_freq='1H'
			period=FREQ+1
	
	
	rng=pd.date_range(start_date+datetime.timedelta(hours=1),freq=resample_freq,periods=period)
	df_new.index=pd.DatetimeIndex(df_new.index)
	test=df_new.reindex(rng,fill_value=0).fillna(0)

	test['date']=test.index.strftime("%d-%b")
	test['time']=test.index.strftime("%I:%M %p")
	test['year']=test.index.year
	test['year']=test['year'].apply(str)
	output['topstats']={"tweets_count":tweets_count,"users_count":users_count,"tweets_per_hour":tweets_per_hour,"sentiment":sentiment}
	output['timestamp_count']=eval(test.to_json(orient='records'))

	output['alertid']=key
	output['bucket']=str(FREQ)+'hour'

	posts=column.find_one({"alertid":key})

	if posts is not None:
		diff_data=get_diff_data(FREQ,diff_days,posts,sentiment,tweets_count,users_count,tweets_per_hour)
		diff_tweets=diff_data[0]
		diff_users=diff_data[1]
		diff_tweets_hour=diff_data[2]
		old_senti=diff_data[3]
		new_senti=diff_data[4]
		output['diff']={"diff_tweets":diff_tweets,"diff_users":diff_users,"diff_tweets_hour":diff_tweets_hour,"diff_senti":{"old_senti":old_senti,"new_senti":new_senti}}
		output['topstats']['last_updated']=str(datetime.datetime.now())
		column.update({'_id': ObjectId(str(posts['_id']))},{'$set':output})
	
	
	else:
		output["diff"]={"diff_tweets":0,"diff_users":0,"diff_tweets_hour":0,"diff_senti":{"old_senti":0,"new_senti":0}}
		output['topstats']['last_updated']=str(datetime.datetime.now())
		column.insert(output)

def write_all_data(alert_info,enddate):
	# for data in sql_output:
	# 	key=data['key']
		

	print ".............Start...........", alert_info
	# get_one_hour(enddate,key,user,track_time)
	# get_six_hours(enddate,key,user,track_time)
	# get_twelve_hours(enddate,key,user,track_time)
	# get_twentyFour_hours(enddate,key,user,track_time)
	get_hourly_data(alert_info,enddate,1,'lastOneHour')
	get_hourly_data(alert_info,enddate,6,'lastSixHours')
	get_hourly_data(alert_info,enddate,12,'lastTwelveHours')
	get_hourly_data(alert_info,enddate,24,'lastTwentyFourHours')
	get_daily_data(alert_info,enddate,7,'lastSevenDays')
	get_daily_data(alert_info,enddate,15,'lastFifteenDays')
	get_daily_data(alert_info,enddate,30,'lastThirtyDays')
	print "................END..........", alert_info


if __name__ == '__main__':
	print "STARTED EVERY 10 MIN JOB"
	curTime=datetime.datetime.now()
	sql_data=[]
	sql_output=get_sql_data(sql_data)

	logging.info("_____START-TIME_________ is %s",str(datetime.datetime.now()))
	start=0
	end=start+10
	# write_all_data({"alert":1379,"track_time":datetime.datetime(2017,1,25,7,45,0)},curTime)
	while start<=len(sql_output):
		print start,end
		parmap.map(write_all_data,sql_output[start:end],curTime)
		if start>=len(sql_output):
			break
		start=end
		end=start+10
		if end>len(sql_output):
			end=len(sql_output)
	
	print str(datetime.datetime.now())
	logging.info("_____END-TIME_________ is %s",str(datetime.datetime.now()))

	print "COMPLETED EVERY 10 MIN JOB"
  