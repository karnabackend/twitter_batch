from pymongo import MongoClient
import datetime
import re
import json
import logging
import traceback
import os
import time
from pd_social_media_keywords import pd_social_media_keywords

from bson.objectid import ObjectId


from twitter_config import *
import pika
queue_name=os.environ['RabbitMQ_twitter']
print "queue",queue_name
parameters = pika.URLParameters('amqp://ankit:root53@146.148.71.201:5672/dev')
connection = pika.BlockingConnection(parameters=parameters)
channel = connection.channel()
channel.queue_declare(queue=queue_name, durable=True)
logging.basicConfig(filename='/home/ankit/twitter_batch/tweets_filter.log',level=logging.INFO)

def MapCalculator(jsonData):
	tempList = []
	for key,val in jsonData.iteritems():
		for k,v in val.iteritems():
			if v >1:
				for r in range(0,v):
					tempList.append(k)
			else:
				tempList.append(k)
	return tempList
def NestedListFunction(_listkey,sentimentsVal):
	_newTemp =[]
	if sentimentsVal>=0.7:
		senti = "positive"
	elif sentimentsVal<=0.3:
		senti = "negative"
	else:
		senti = "neutral"

	if len(_listkey)==0:
		return _newTemp
	else:
		for r in range(len(_listkey)):
			_tempList = []
			_tempList.append(_listkey[r])
			_tempList.append(senti)
			_newTemp.append(_tempList)
		return _newTemp

def callback(ch, method, properties, body):
	body=json.loads(body)
	# print body
	
	# users_list=body.get('subscribers')
	language=body.get('tweet').get('language')
	alert_id_list=body.get('alertid')
	current_time=datetime.datetime.now()
	retweet_trend={}
	
	print "queue has subscriber.."
	for alert in alert_id_list:
		tweet_tracked=False
		post_check=semantic_db.find_one({"alertid":alert,"tweet_id":body.get('tweet').get('id')})
		if post_check is None:
			post_check=semantic_db.find_one({"alertid":alert,"tweet_id":body.get('retweet').get('original_id')})
		if post_check is None:
		
			tweet_id=body.get('retweet').get('original_id') or body['tweet']['id']
			timestamp=body.get('retweet').get('timestamp') or body.get('tweet').get('timestamp')
			tweet_user=body.get('retweet').get('user') or body['tweet']['user']
			favorite_count=body.get('retweet').get('favorite_count') or body['tweet']['favorite_count']
			retweet_count=body['tweet']['retweet_count']
			
			sentiment=body['sentiment']
			text=body.get('retweet').get('original_text') or body['tweet']['text']
			pattern = re.compile('RT @[a-z,A-Z0-9\-\_\+\#\$\*\!]*:')
			text=re.sub(pattern,'',text)
			entity=body['tweet']['entity']
			status=body['tweet']['tweet_status']
			emotionType = body['tweet']['emotionType']
			location = body['location']
			logging.info('processing the post tweetID %s which is %s',str(tweet_id),str(status))
			

			try:

				keyWords 							= body.get('tweet').get('keywords')
				# keyWords 							=[]
				# keyWordsMap 						= []
				# _lists								= MapCalculator(keyWords) 
				# keyWords  							=_lists
				# keyWordsMap 						= NestedListFunction(_lists,sentiment)
			except Exception as e:
				print e, text
				keyWordsMap=[]
				keyWords=[]

			tweet_link=body["tweet"]["tweet_link"].replace("http://","")

			if status=='post':
				retw_count_tracked=0
				retweeted_users=[]
			else:

				# retw_count_tracked=tweets_db.find({"alertid":{"$in":[alert]},"retweet.original_id":tweet_id}).count()
				retweeted_users=[{"username":body['tweet']['user']['name'],"screen_name":body['tweet']['user']['screen_name'],"timestamp":body['tweet']['timestamp'],"followers_count":body['tweet']['user']['followersCount'],"profile_image_url":body['tweet']['user']['image'],'reuser_id':body['tweet']['user']['id']}]
				retw_count_tracked=len(retweeted_users)
				retw_timestamp=datetime.datetime.strptime(body['tweet']['timestamp'],"%Y-%m-%d %H:%M:%S").date()
				if str(retw_timestamp) not in retweet_trend:
					retweet_trend[str(retw_timestamp)]=1
				else:
					retweet_trend[str(retw_timestamp)]+=1

			popularity_score_tracked=retw_count_tracked
			output={}
			# current_time=datetime.datetime.now()
			output={"flag":False,"favorites":False,"tagname":body.get('tagname'),"alertid":alert,"tweet_id":tweet_id,"keywords":keyWords,"tags":[],"sentiment":sentiment,"language":language,"text":text,"location":location,"textType":body['tweet']['textType'], "emotionType":emotionType,"url":tweet_link,"timestamp":timestamp,"retw_count_tracked":retw_count_tracked,"favorite_count":favorite_count,"retweet_count":retweet_count,"user":tweet_user,"popularity_score":retweet_count,"retweeted_users":retweeted_users,"entity":entity,"receive_time":current_time,"popularity_score_tracked":popularity_score_tracked,"retweet_trend":retweet_trend}
			post_check=semantic_db.find_one({"alertid":alert,"tweet_id":tweet_id})
			
			if post_check is None:
				semantic_db.insert(output)
				logging.info('inserting the post tweetID %s which is %s',str(tweet_id),str(status))
			else:
				tweet_tracked=True

		else:
			tweet_tracked=True

			
		if tweet_tracked==True:
			max_score=post_check['popularity_score']
			max_favorite_count=post_check['favorite_count']
			max_retweet_count=post_check['retweet_count']
			max_popularity_score_tracked=post_check.get('popularity_score_tracked')
			
			retw_timestamp=datetime.datetime.strptime(body['tweet']['timestamp'],"%Y-%m-%d %H:%M:%S").date()
			retweet_trend=post_check.get('retweet_trend',{})
			if str(retw_timestamp) not in retweet_trend:
				retweet_trend[str(retw_timestamp)]=1
			else:
				retweet_trend[str(retw_timestamp)]+=1
			
			# current_time=datetime.datetime.now()

			# retw_count_tracked=tweets_db.find({"alertid":{"$in":[alert]},"retweet.original_id":post_check['tweet_id']}).count()
			favorite_count=body.get('retweet').get('favorite_count') or body['tweet']['favorite_count']
			if (body['tweet']['retweet_count'])>=max_score:
				max_score=(body['tweet']['retweet_count'])
				max_retweet_count=body['tweet']['retweet_count']
				max_favorite_count=favorite_count
			
			
			
			retweeted_users=post_check['retweeted_users']+[{"username":body['tweet']['user']['name'],"screen_name":body['tweet']['user']['screen_name'],"timestamp":body['tweet']['timestamp'],"followers_count":body['tweet']['user']['followersCount'],"profile_image_url":body['tweet']['user']['image'],'reuser_id':body['tweet']['user']['id']}]
			retw_count_tracked=len(retweeted_users)
			if retw_count_tracked>=max_popularity_score_tracked:
				max_popularity_score_tracked=retw_count_tracked
			# retweeted_users=[dict(t) for t in set([tuple(d.items()) for d in retweeted_users])]
			semantic_db.update({"alertid":alert,"tweet_id":post_check['tweet_id']},{"$set":{"favorite_count":max_favorite_count,"retweet_count":max_retweet_count,"receive_time":current_time,"alertid":alert,"retweeted_users":retweeted_users,"popularity_score":max_score,"retw_count_tracked":retw_count_tracked,"popularity_score_tracked":max_popularity_score_tracked,"retweet_trend":retweet_trend}})
			logging.info('updating the post tweetID %s',str(post_check['tweet_id']))





	ch.basic_ack(delivery_tag = method.delivery_tag)
	print "finished"



channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback,queue=queue_name)
print(' [*] Waiting for queuuee messages. To exit press CTRL+C')
channel.start_consuming()


