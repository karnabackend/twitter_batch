from pymongo import MongoClient
import datetime
import urllib2
import re
import json
from collections import Counter
import collections
import itertools
import traceback
import ast
import os
import time
import MySQLdb
from bson.objectid import ObjectId
from pd_social_media_keywords import pd_social_media_keywords
from twitter_config import *
from pd_sentiment import pd_sentiment
import gc
import redis
from memsql.common import database
import MySQLdb as db
import MySQLdb.cursors
import parmap
import pandas as pd

red = redis.StrictRedis(host='104.199.138.153', port=6379, db=0)
mongo_url = os.environ['MONGOURL']
mongo_prod_url = os.environ['MONGO_PROD_URL']


def get_sql_connection():
	connection= db.connect(host='memsql.paralleldots.com',user='ankit',passwd='mongodude123',db="socialmedia")     
	return connection

def agebuckets(agedata):
	items = sorted([[int(re.search(r'\d+', key).group()),float(agedata[key])] for key in agedata.keys() if "over" in key],key=lambda k:k[0])
	ageds = []
	if len(items)>0:
		ageds.append(["below %s"%items[0][0],round(100-items[0][1],2)])
		z = zip(items,items[1:])
	for x in z:
		ageds.append(["%s-%s"%(x[0][0],x[1][0]),round(x[0][1]-x[1][1],2)])
	ageds.append(["over %s"%items[-1][0],round(items[-1][1],2)])
	return ageds



def map_country_codes(country):
	country_codes = {'korea deocratic people republic of': 'PRK', 'french southern territories': 'ATF', 'tokelau': 'TKL', 'cayman islands': 'CYM', 'iran, islamic republic of': 'IRN', 'azerbaijan': 'AZE', 'uzbekistan': 'UZB', 'senegal': 'SEN', 'french guiana': 'GUF', 'cambodia': 'KHM', 'guinea-bissau': 'GNB', 'japan': 'JPN', 'cyprus': 'CYP', 'cook islands': 'COK', 'bhutan': 'BTN', 'lithuania': 'LTU', 'mongolia': 'MNG', 'united kingdom': 'GBR', 'tunisia': 'TUN', 'rwanda': 'RWA', 'aruba': 'ABW', 'puerto rico': 'PRI', 'argentina': 'ARG', 'norway': 'NOR', 'sierra leone': 'SLE', 'ghana': 'GHA', 'australia': 'AUS', 'mauritania': 'MRT', 'zambia': 'ZMB', 'lao people democratic republic': 'LAO', 'french polynesia': 'PYF', 'guatemala': 'GTM', 'zimbabwe': 'ZWE', 'andorra': 'AND', 'belgium': 'BEL', 'haiti': 'HTI', 'kazakhstan': 'KAZ', 'burkina faso': 'BFA', 'land islands': 'ALA', 'kyrgyzstan': 'KGZ', 'suriname': 'SUR', 'saint barthlemy': 'BLM', 'denmark': 'DNK', 'philippines': 'PHL', 'montserrat': 'MSR', 'djibouti': 'DJI', 'saint helena, ascension and tristan da cunha': 'SHN', 'latvia': 'LVA', 'namibia': 'NAM', 'bosnia and herzegovina': 'BIH', 'chad': 'TCD', 'sri lanka': 'LKA', 'guinea': 'GIN', 'bulgaria': 'BGR', 'jamaica': 'JAM', 'greenland': 'GRL', 'samoa': 'WSM', 'american samoa': 'ASM', 'lebanon': 'LBN', 'virgin islands, u.s.': 'VIR', 'malaysia': 'MYS', 'falkland islands (malvinas)': 'FLK', 'christmas island': 'CXR', 'mozambique': 'MOZ', 'sint maarten (dutch part)': 'SXM', 'micronesia, federated states of': 'FSM', 'greece': 'GRC', 'nicaragua': 'NIC', 'new zealand': 'NZL', 'canada': 'CAN', 'afghanistan': 'AFG', 'qatar': 'QAT', 'palau': 'PLW', 'turkmenistan': 'TKM', 'equatorial guinea': 'GNQ', 'pitcairn': 'PCN', 'kuwait': 'KWT', 'panama': 'PAN', 'nepal': 'NPL', 'central african republic': 'CAF', 'luxembourg': 'LUX', 'solomon islands': 'SLB', 'netherlands': 'NLD', 'somalia': 'SOM', 'iceland': 'ISL', 'croatia': 'HRV', 'nauru': 'NRU', 'saint lucia': 'LCA', 'south georgia and the south sandwich islands': 'SGS', 'british indian ocean territory': 'IOT', 'united arab emirates': 'ARE', 'guyana': 'GUY', 'saudi arabia': 'SAU', 'saint kitts and nevis': 'KNA', 'switzerland': 'CHE', 'paraguay': 'PRY', 'china': 'CHN', 'armenia': 'ARM', 'grenada': 'GRD', 'kiribati': 'KIR', 'belize': 'BLZ', 'syrian arab republic': 'SYR', 'ctedivoire': 'CIV', 'bangladesh': 'BGD', 'palestine, state of': 'PSE', 'guadeloupe': 'GLP', 'northern mariana islands': 'MNP', 'libya': 'LBY', 'trinidad and tobago': 'TTO', 'mayotte': 'MYT', 'oman': 'OMN', 'finland': 'FIN', 'gambia': 'GMB', 'saint pierre and miquelon': 'SPM', 'mauritius': 'MUS', 'marshall islands': 'MHL', 'niue': 'NIU', 'dominican republic': 'DOM', 'jersey': 'JEY', 'bahamas': 'BHS', 'korea, republic of': 'KOR', 'pakistan': 'PAK', 'romania': 'ROU', 'seychelles': 'SYC', 'czech republic': 'CZE', 'myanmar': 'MMR', 'el salvador': 'SLV', 'egypt': 'EGY', 'guam': 'GUM', 'papua new guinea': 'PNG', 'united states': 'USA', 'austria': 'AUT', 'congo': 'COG', 'hungary': 'HUN', 'colombia': 'COL', 'thailand': 'THA', 'angola': 'AGO', 'niger': 'NER', 'korea, democratic people republic of': 'PRK', 'fiji': 'FJI', 'comoros': 'COM', 'turkey': 'TUR', 'curaao': 'CUW', 'madagascar': 'MDG', 'iraq': 'IRQ', 'estonia': 'EST', 'mexico': 'MEX', 'france': 'FRA', 'macedonia, the former yugoslav republic of': 'MKD', 'slovakia': 'SVK', 'gibraltar': 'GIB', 'swaziland': 'SWZ', 'nigeria': 'NGA', 'anguilla': 'AIA', 'malawi': 'MWI', 'ecuador': 'ECU', 'moldova, republic of': 'MDA', 'ukraine': 'UKR', 'israel': 'ISR', 'congo, the democratic republic of the': 'COD', 'albania': 'ALB', 'peru': 'PER', 'western sahara': 'ESH', 'serbia': 'SRB', 'georgia': 'GEO', 'montenegro': 'MNE', 'tajikistan': 'TJK', 'svalbard and jan mayen': 'SJM', 'togo': 'TGO', 'cted ivoire': 'CIV', 'holy see (vatican city state)': 'VAT', 'jordan': 'JOR', 'chile': 'CHL', 'martinique': 'MTQ', 'turks and caicos islands': 'TCA', 'virgin islands, british': 'VGB', 'spain': 'ESP', 'sao tome and principe': 'STP', 'isle of man': 'IMN', 'bouvet island': 'BVT', 'viet nam': 'VNM', 'brunei darussalam': 'BRN', 'morocco': 'MAR', 'sweden': 'SWE', 'heard island and mcdonald islands': 'HMD', 'gabon': 'GAB', 'mali': 'MLI', 'russian federation': 'RUS', 'runion': 'REU', 'yemen': 'YEM', 'hong kong': 'HKG', 'bahrain': 'BHR', 'portugal': 'PRT', 'uruguay': 'URY', 'reunion': 'REU', 'india': 'IND', 'new caledonia': 'NCL', 'lesotho': 'LSO', 'antarctica': 'ATA', 'belarus': 'BLR', 'saint vincent and the grenadines': 'VCT', 'south sudan': 'SSD', 'uganda': 'UGA', 'burundi': 'BDI', 'kenya': 'KEN', 'ireland': 'IRL', 'macao': 'MAC', 'botswana': 'BWA', 'saint martin (french part)': 'MAF', 'italy': 'ITA', 'algeria': 'DZA', 'south africa': 'ZAF', 'cuba': 'CUB', 'malta': 'MLT', 'liberia': 'LBR', 'ethiopia': 'ETH', 'bermuda': 'BMU', 'timor-leste': 'TLS', 'vanuatu': 'VUT', 'antigua and barbuda': 'ATG', 'cameroon': 'CMR', 'benin': 'BEN', 'brazil': 'BRA', 'tanzania, united republic of': 'TZA', 'singapore': 'SGP', 'faroe islands': 'FRO', 'tuvalu': 'TUV', 'san marino': 'SMR', 'monaco': 'MCO', 'costa rica': 'CRI', 'venezuela, bolivarian republic of': 'VEN', 'united states minor outlying islands': 'UMI', 'cocos (keeling) islands': 'CCK', 'slovenia': 'SVN', 'honduras': 'HND', 'germany': 'DEU', 'wallis and futuna': 'WLF', 'dominica': 'DMA', 'eritrea': 'ERI', 'tonga': 'TON', 'maldives': 'MDV', 'norfolk island': 'NFK', 'poland': 'POL', 'indonesia': 'IDN', 'cape verde': 'CPV', 'taiwan, province of china': 'TWN', 'bonaire, sint eustatius and saba': 'BES', 'sudan': 'SDN', 'liechtenstein': 'LIE', 'guernsey': 'GGY', 'bolivia, plurinational state of': 'BOL', 'barbados': 'BRB'}

	return country_codes[country.lower()]



def get_sql_data(sql_data):
	
	conn=get_sql_connection()
	cursorobj=conn.cursor()
	cursorobj.execute("Select id,alert_status,modified_time,twitter_status from alerts where twitter=1 order by id desc")
	for row in cursorobj.fetchall():

		sql_data.append({"tw_status":row[3],"status":row[1],"alert_id":row[0],"modified_time":row[2]})
	conn.close()

	return sql_data

		# print "userIds",userids

	# for user in userids:
	# 	if user is not None:

			
	# 		conn=get_sql_connection()
	# 		conn.set_character_set('utf8')
	# 		cursorobj=conn.cursor()
	# 		cursorobj.execute("Select id,alert_status,tw_track_start_time,modified_time,twitter_status from alerts where twitter=1 and uid=%s",[user])
	# 		for row in cursorobj.fetchall():
	# 			key=row[0]
	# 			status=row[1]
	# 			track_time=row[2]
	# 			modified_time=row[3]
	# 			twitter_status=row[4]
			

	# 			sql_data.append({"user":user,"key":key,"track_time":track_time,"tw_status":twitter_status,"status":status,"alert_id":key,"modified_time":modified_time})
	

# @profile
def get_counter_data(alert,tweets_analysis):
	post_count=share_count=noneng_post_count=noneng_share_count=0
	if tweets_analysis.empty==False:

		eng_tweets_analysis=tweets_analysis[tweets_analysis['tweet_lang']=='en']
		tweets_count=tweets_analysis.groupby('tweet_type')['tweet_type']
		post_count=share_count=noneng_post_count=noneng_share_count=0
		if 'post' in tweets_count.groups:
			post_count=tweets_count.get_group('post').count()
		if 'share' in tweets_count.groups:
			share_count=tweets_count.get_group('share').count()

		noneng_tweets_analysis=tweets_analysis[tweets_analysis['tweet_lang']!='en']
		noneng_tweets_count=noneng_tweets_analysis.groupby('tweet_type')['tweet_type']
		if 'post' in noneng_tweets_count.groups:
			noneng_post_count=noneng_tweets_count.get_group('post').count()
		if 'share' in noneng_tweets_count.groups:
			noneng_share_count=noneng_tweets_count.get_group('share').count()

	# print post_count,share_count,noneng_post_count,noneng_share_count
	return post_count,share_count,noneng_post_count,noneng_share_count


# @profile
def get_stats_data(alert,tweets_analysis):
	pos_count=neg_count=neutral_count=noneng_tweets_count=0
	if tweets_analysis.empty==False:

		eng_tweets_analysis=tweets_analysis[tweets_analysis['tweet_lang']=='en']
		pos_count=eng_tweets_analysis[(tweets_analysis['sentiment']>=0.7)]['sentiment'].count()
		neg_count=eng_tweets_analysis[(tweets_analysis['sentiment']<=0.3)]['sentiment'].count()
		neutral_count=eng_tweets_analysis[(tweets_analysis['sentiment']>0.3) & (tweets_analysis['sentiment']<0.7)]['sentiment'].count()

		noneng_tweets_analysis=tweets_analysis[tweets_analysis['tweet_lang']!='en']
		noneng_tweets_count=noneng_tweets_analysis['tweet_type'].count()

	# print pos_count,neg_count,neutral_count,noneng_tweets_count
	return pos_count,neg_count,neutral_count,noneng_tweets_count

def overall_influencersForHashtag_v2(hashtag):
	try:
		semantic_db=MongoClient(mongo_url,27017)["dev_twitter"]["semantic_data"]
		original_data=semantic_db.find({"alertid":hashtag}).sort("popularity_score_tracked",-1).limit(100)
		influencers_list={}
		for data in original_data:
			user_id=data.get('user').get('id')
			if user_id not in influencers_list:

				influencers_list[user_id]={"profile":"https://twitter.com/"+data['user']['screen_name'],"handle":data['user']['screen_name'],"name":data['user']['name'],"img_url":data['user']['image'],"score":data.get('popularity_score_tracked',0),"txt_id":[str(data['tweet_id'])],"followers_count":data['user']['followersCount'],"tweets_count":1,"retweets_count":data.get('retw_count_tracked',0),"likes_count":data['favorite_count'],"is_verified":data.get('user').get('verified')}
			else:
				updated_score=influencers_list[user_id]['score']+data.get('popularity_score_tracked',0)
				txt_id=influencers_list[user_id]['txt_id']+[str(data['tweet_id'])]
			
				retweets_count=influencers_list[user_id]['retweets_count']+data.get('retw_count_tracked',0)
				likes_count=influencers_list[user_id]['likes_count']+data['favorite_count']
				tweets_count=influencers_list[user_id]['tweets_count']+1
				followers_count=max(influencers_list[user_id]['followers_count'],data['user']['followersCount'])
				influencers_list[user_id]={"profile":"https://twitter.com/"+data['user']['screen_name'],"handle":data['user']['screen_name'],"name":data['user']['name'],"img_url":data['user']['image'],"score":updated_score,"txt_id":txt_id,"followers_count":followers_count,"tweets_count":tweets_count,"retweets_count":retweets_count,"likes_count":likes_count,"is_verified":data.get('user').get('verified')}

		influencers=[]
		for infl in influencers_list:
			if influencers_list[infl]['tweets_count']!=0:
				influencers_list[infl]['avg_retweets']=influencers_list[infl]['retweets_count']/influencers_list[infl]['tweets_count']
				influencers_list[infl]['avg_likes']=influencers_list[infl]['likes_count']/influencers_list[infl]['tweets_count']
			else:
				influencers_list[infl]['avg_retweets']=0
				influencers_list[infl]['avg_likes']=0

			influencers.append(influencers_list[infl])

		
		influencers = sorted(influencers,key=lambda x:-x["score"])[:20]
		return influencers
	except Exception as error:
		print traceback.format_exc()
		return []


def eng_influencersForHashtag_v2(hashtag):
	try:
		semantic_db=MongoClient(mongo_url,27017)["dev_twitter"]["semantic_data"]
		original_data=semantic_db.find({"alertid":hashtag,"language":'en'}).sort("popularity_score_tracked",-1).limit(100)
		influencers_list={}

		for data in original_data:
			user_id=data.get('user').get('id')
			if user_id not in influencers_list:
				if data['sentiment']>=0.7:
					sentiment='Positive'
				elif data['sentiment']<=0.3:
					sentiment='Negative'
				else:
					sentiment='Neutral'
				influencers_list[user_id]={"profile":"https://twitter.com/"+data['user']['screen_name'],"handle":data['user']['screen_name'],"name":data['user']['name'],"img_url":data['user']['image'],"sentiment":[sentiment],"score":data.get('popularity_score_tracked',0),"txt_id":[str(data['tweet_id'])],"followers_count":data['user']['followersCount'],"tweets_count":1,"retweets_count":data.get('retw_count_tracked',0),"likes_count":data['favorite_count'],"is_verified":data.get('user').get('verified')}
			else:
				updated_score=influencers_list[user_id]['score']+data.get('popularity_score_tracked',0)
				txt_id=influencers_list[user_id]['txt_id']+[str(data['tweet_id'])]
				# print txt_id
				if data['sentiment']>=0.7:
					sentiment='Positive'
				elif data['sentiment']<=0.3:
					sentiment='Negative'
				else:
					sentiment='Neutral'
				retweets_count=influencers_list[user_id]['retweets_count']+data.get('retw_count_tracked',0)
				likes_count=influencers_list[user_id]['likes_count']+data['favorite_count']
				tweets_count=influencers_list[user_id]['tweets_count']+1
				followers_count=max(influencers_list[user_id]['followers_count'],data['user']['followersCount'])
				influencers_list[user_id]={"profile":"https://twitter.com/"+data['user']['screen_name'],"handle":data['user']['screen_name'],"name":data['user']['name'],"img_url":data['user']['image'],"sentiment":[sentiment]+influencers_list[user_id]['sentiment'],"score":updated_score,"txt_id":txt_id,"followers_count":followers_count,"tweets_count":tweets_count,"retweets_count":retweets_count,"likes_count":likes_count,"is_verified":data.get('user').get('verified')}

		influencers=[]
		for infl in influencers_list:
			influencers_list[infl]['sentiment']=Counter(influencers_list[infl]['sentiment']).most_common(1)[0][0]
			if influencers_list[infl]['tweets_count']!=0:
				influencers_list[infl]['avg_retweets']=influencers_list[infl]['retweets_count']/influencers_list[infl]['tweets_count']
				influencers_list[infl]['avg_likes']=influencers_list[infl]['likes_count']/influencers_list[infl]['tweets_count']
			else:
				influencers_list[infl]['avg_retweets']=0
				influencers_list[infl]['avg_likes']=0

			influencers.append(influencers_list[infl])

		
		influencers = sorted(influencers,key=lambda x:-x["score"])[:20]
		# print influencers
		return influencers
	except Exception as error:
		print traceback.format_exc()
		return []

def hashtagsForTweets_v2(tagname):
	try:
		conn=get_sql_connection()
		cursorobj=conn.cursor()
		conn.set_character_set('utf8')
		cursorobj.execute("Select tw_track_words from alerts where id=%s",[int(tagname)])
		for row in cursorobj.fetchone():
			query_hashtag=row

		conn.close()
	except:
		print traceback.format_exc()
	try:
		semantic_db=MongoClient(mongo_url,27017)["dev_twitter"]["semantic_data"]
		hashtags_data=semantic_db.aggregate([{"$match":{"alertid":tagname}},{"$unwind":"$entity.hashtags"},{"$group":{"_id":'$entity.hashtags.text',"positive":{"$sum":{"$cond":[{"$gt":["$sentiment",0.7]},1,0]}},"negative":{"$sum":{"$cond":[{"$lt":["$sentiment",0.3]},1,0]}},"neutral":{"$sum":{"$cond":[{"$and": [{"$gt":["$sentiment",0.3]},{"$lt":["$sentiment",0.7]}]},1,0]}},"count":{"$sum":{"$multiply":[1,"$retw_count_tracked"]}}}},{"$sort":{"count":-1}},{"$limit":75}])
		hashtags={}
		check_hashtags={}
		for hashtag in hashtags_data:
			# print hashtag
			max_count=max(hashtag['positive'],hashtag['negative'],hashtag['neutral'])
			if max_count==hashtag['positive']:
				sentiment='positive'
			elif max_count==hashtag['negative']:
				sentiment='negative'
			else:
				sentiment='neutral'
			if hashtag['_id'].lower() not in hashtags.keys():
				hashtags[hashtag['_id'].lower()]={"counter":hashtag['count'],"sentiment":sentiment}
				check_hashtags[hashtag['_id'].lower()]={"key":hashtag['_id'],"count":hashtag['count']}
			else:
				hashtags[hashtag['_id'].lower()]['counter']+=hashtag['count']
				if hashtag['count']>check_hashtags[hashtag['_id'].lower()]['count']:
					check_hashtags[hashtag['_id'].lower()]={"key":hashtag['_id'],"count":hashtag['count']}

		final_hashtags={}
		# print hashtags
		for hashtag in hashtags:

			tag=check_hashtags[hashtag]['key']
			if tag.lower()!=query_hashtag.lower():
				final_hashtags[tag]=hashtags[hashtag]


		# final_hashtags=sorted(final_hashtags.items(),key=lambda x: x[1]['counter'],reverse=True)[:75]
		# final_hashtags=tuple(final_hashtags)
		# final_hashtags=dict(final_hashtags)
	# print final_hashtags
		# print final_hashtags
		return final_hashtags
	except Exception as error:
		print traceback.format_exc()
		return {}

def keywords_analysis(tagname):
	try:
		conn=get_sql_connection()
		cursorobj=conn.cursor()
		conn.set_character_set('utf8')
		cursorobj.execute("Select tw_track_words from alerts where id=%s",[int(tagname)])
		
		for row in cursorobj.fetchone():
			query_hashtag=row

		conn.close()
	except:
		print traceback.format_exc()
	try:
		semantic_db=MongoClient(mongo_url,27017)["dev_twitter"]["semantic_data"]
		keywords_data= semantic_db.aggregate([{"$match":{"alertid":tagname}},{"$unwind":"$keywords"},{"$group":{"_id":'$keywords',"positive":{"$sum":{"$cond":[{"$gt":["$sentiment",0.7]},1,0]}},"negative":{"$sum":{"$cond":[{"$lt":["$sentiment",0.3]},1,0]}},"neutral":{"$sum":{"$cond":[{"$and": [{"$gt":["$sentiment",0.3]},{"$lt":["$sentiment",0.7]}]},1,0]}},"count":{"$sum":{"$multiply":[1,"$retw_count_tracked"]}}}},{"$sort":{"count":-1}},{"$limit":80}])
		keywords={}
		check_keywords={}
		for key in keywords_data:
			if key["_id"]!="https":
				max_count=max(key['positive'],key['negative'],key['neutral'])
				if max_count==key['positive']:
					sentiment='positive'
				elif max_count==key['negative']:
					sentiment='negative'
				else:
					sentiment='neutral'

				if key['_id'].lower() not in keywords.keys():
					keywords[key['_id'].lower()]={"score":key['count'],"sentiment":sentiment}
					check_keywords[key['_id'].lower()]={"key":key['_id'],"count":key['count']}
				else:
					keywords[key['_id'].lower()]['score']+=key['count']
					if key['count']>check_keywords[key['_id'].lower()]['count']:
						check_keywords[key['_id'].lower()]={"key":key['_id'],"count":key['count']}

		final_keywords={}
		for key in keywords:
			tag=check_keywords[key]['key']
			if tag.lower()!=query_hashtag.lower():
				final_keywords[tag.replace('.','').replace('$','')]=keywords[key]
			if len(final_keywords)==75:
				break
		# print keywords,'________________',final_keywords
		return final_keywords
	except Exception as error:
		print traceback.format_exc() 
		return {} 


def hashtags_excluded_retweets(hashtag):
	try:
		conn=get_sql_connection()
		cursorobj=conn.cursor()
		conn.set_character_set('utf8')
		cursorobj.execute("Select tw_track_words from alerts where id=%s",[int(hashtag)])
		for row in cursorobj.fetchone():
			query_hashtag=row

		conn.close()
	except:
		print traceback.format_exc()

	try:
		semantic_db=MongoClient(mongo_url,27017)["dev_twitter"]["semantic_data"]
		hashtags_data=semantic_db.aggregate([{"$match":{"alertid":hashtag}},{"$unwind":"$entity.hashtags"},{"$group":{"_id":'$entity.hashtags.text',"positive":{"$sum":{"$cond":[{"$gt":["$sentiment",0.7]},1,0]}},"negative":{"$sum":{"$cond":[{"$lt":["$sentiment",0.3]},1,0]}},"neutral":{"$sum":{"$cond":[{"$and": [{"$gt":["$sentiment",0.3]},{"$lt":["$sentiment",0.7]}]},1,0]}},"count":{"$sum":1}}},{"$sort":{"count":-1}},{"$limit":75}])
		hashtags={}
		check_hashtags={}
		for hashtag in hashtags_data:
			# print hashtag
			max_count=max(hashtag['positive'],hashtag['negative'],hashtag['neutral'])
			if max_count==hashtag['positive']:
				sentiment='positive'
			elif max_count==hashtag['negative']:
				sentiment='negative'
			else:
				sentiment='neutral'
			if hashtag['_id'].lower() not in hashtags.keys():
				hashtags[hashtag['_id'].lower()]={"counter":hashtag['count'],"sentiment":sentiment}
				check_hashtags[hashtag['_id'].lower()]={"key":hashtag['_id'],"count":hashtag['count']}
			else:
				hashtags[hashtag['_id'].lower()]['counter']+=hashtag['count']
				if hashtag['count']>check_hashtags[hashtag['_id'].lower()]['count']:
					check_hashtags[hashtag['_id'].lower()]={"key":hashtag['_id'],"count":hashtag['count']}

		final_hashtags={}
		# print hashtags
		for hashtag in hashtags:

			tag=check_hashtags[hashtag]['key']
			if tag.lower()!=query_hashtag.lower():
				final_hashtags[tag]=hashtags[hashtag]


		# final_hashtags=sorted(final_hashtags.items(),key=lambda x: x[1]['counter'],reverse=True)[:75]
		# final_hashtags=tuple(final_hashtags)
		# final_hashtags=dict(final_hashtags)
	# print final_hashtags
		return final_hashtags
	except Exception as error:
		print traceback.format_exc()
		return {}



def keywords_excluded_retweets(tagname):
	try:
		conn=get_sql_connection()
		cursorobj=conn.cursor()
		conn.set_character_set('utf8')
		cursorobj.execute("Select tw_track_words from alerts where id=%s",[int(tagname)])
		
		for row in cursorobj.fetchone():
			query_hashtag=row

		conn.close()
	except:
		print traceback.format_exc()
	try:
		semantic_db=MongoClient(mongo_url,27017)["dev_twitter"]["semantic_data"]
		keywords_data= semantic_db.aggregate([{"$match":{"alertid":tagname}},{"$unwind":"$keywords"},{"$group":{"_id":'$keywords',"positive":{"$sum":{"$cond":[{"$gt":["$sentiment",0.7]},1,0]}},"negative":{"$sum":{"$cond":[{"$lt":["$sentiment",0.3]},1,0]}},"neutral":{"$sum":{"$cond":[{"$and": [{"$gt":["$sentiment",0.3]},{"$lt":["$sentiment",0.7]}]},1,0]}},"count":{"$sum":1}}},{"$sort":{"count":-1}},{"$limit":75}])
		keywords={}
		check_keywords={}
		for key in keywords_data:
			if key["_id"]!="https":
				max_count=max(key['positive'],key['negative'],key['neutral'])
				if max_count==key['positive']:
					sentiment='positive'
				elif max_count==key['negative']:
					sentiment='negative'
				else:
					sentiment='neutral'

				if key['_id'].lower() not in keywords.keys():
					keywords[key['_id'].lower()]={"score":key['count'],"sentiment":sentiment}
					check_keywords[key['_id'].lower()]={"key":key['_id'],"count":key['count']}
				else:
					keywords[key['_id'].lower()]['score']+=key['count']
					if key['count']>check_keywords[key['_id'].lower()]['count']:
						check_keywords[key['_id'].lower()]={"key":key['_id'],"count":key['count']}

		final_keywords={}
		for key in keywords:
			tag=check_keywords[key]['key']
			if tag.lower()!=query_hashtag.lower():
				final_keywords[tag.replace('.','').replace('$','')]=keywords[key]
			if len(final_keywords)==75:
				break
		# print keywords,'________________',final_keywords
		return final_keywords
	except Exception as error:
		print traceback.format_exc() 
		return {} 

def get_location(tagname,country):
	try:
		tweets_db = MongoClient(mongo_url,27017)["GnipDataFinal"]["tweets"]
		pipe = [{"$match":{"$and":[{"location.country":re.compile(country, re.IGNORECASE)},{"location.region":{"$exists":1}},{"alertid":{"$in":[tagname]}}]}},{"$group":{"_id":"$location.region","positive":{"$sum":{"$cond":[{"$gt":["$sentiment",0.7]},1,0]}},"negative":{"$sum":{"$cond":[{"$lt":["$sentiment",0.3]},1,0]}},"neutral":{"$sum":{"$cond":[{"$and": [{"$gt":["$sentiment",0.3]},{"$lt":["$sentiment",0.7]}]},1,0]}}}},{"$sort":{"neutral":-1,"positive":-1,"negative":-1}},{"$limit":11}]
			
		cursor = tweets_db.aggregate(pipeline=pipe)
		final = {}
		data = {}
		# print "========================================================="
		count = 0

		total_tweets = tweets_db.find({"$and":[{"location.country":re.compile(country, re.IGNORECASE)},{"location.region":{"$exists":1}},{"alertid":{"$in":[tagname]}}]}).count()
		# print total_tweets
		total_tweets = round(total_tweets,2)
		
		for cur in cursor:
			#print cur
			data_dict = {}
			total = 0
			positive = cur["positive"]
			negative = cur["negative"]
			neutral = cur["neutral"]
			location = cur["_id"]
			# count+=1
			total = int(negative+neutral+positive)
			total = round(total,2)
			if location is not None:

				location=location.replace('.','')
				percentage = round(((total/total_tweets)*100),2)
				check = round((0.1*total_tweets),2)
				maximum = max(positive,neutral,negative)
				if maximum == positive:
					sentiment ="positive"
				elif maximum == negative:
					sentiment = "negative"
				elif maximum == neutral:
					sentiment = "neutral"

				data_dict["sentiment"] = sentiment
				data_dict["percentage"] = percentage
				data_dict["positive"] = positive
				data_dict["negative"] = negative
				data_dict["neutral"] = neutral
				# data_list.append(sentiment)
				# data_list.append(percentage)
				data[location] = data_dict
			else:
				total_tweets=total_tweets-total
				total_tweets = round(total_tweets,2)

		final_list = []
		for key, value in sorted(data.iteritems(), key=lambda x: x[1]["percentage"],reverse=True):
			#print "%s: %s" % (key, value)
			final_data = {}
			final_data[key] = value
			final_list.append(final_data)

		return {"country":country,"regions":final_list}
	except Exception as error:
		print traceback.format_exc()
		return []


def get_countries(key):
	try:
		tweets_db = MongoClient(mongo_url,27017)["GnipDataFinal"]["tweets"]
		countries_agg=tweets_db.aggregate([{"$match":{"alertid":{"$in":[key]},"location.country":{"$exists":1}}},{"$group":{"_id":"$location.country","positive":{"$sum":{"$cond":[{"$gt":["$sentiment",0.7]},1,0]}},"negative":{"$sum":{"$cond":[{"$lt":["$sentiment",0.3]},1,0]}},"neutral":{"$sum":{"$cond":[{"$and": [{"$gt":["$sentiment",0.3]},{"$lt":["$sentiment",0.7]}]},1,0]}}}},{"$project":{"_id":1,"positive":1,"negative":1,"neutral":1,"count":{"$add":['$positive','$negative','$neutral']}}},{"$sort":{"count":-1}},{"$limit":10}])
		tweets_count=tweets_db.find({"alertid":{"$in":[key]},"location.country":{"$exists":1}}).count()
		countries=[]
		check_countries=[]
		out_countries={}
		output={}
		
		for c in countries_agg:
			# print c
			if c['_id'] is not None:
				max_val=[c['positive'],c['negative'],c['neutral']]
				sentiment=['positive','negative','neutral'][max_val.index(max(max_val))]
				percent_value=c['count']*100/float(tweets_count)
				percent_value=round(percent_value,2)
				country=map_country_codes(c['_id'])
				check_countries.append(c['_id'])
				out_countries[country.replace('.','')]={"percent":percent_value,"sentiment":sentiment}
				
	except Exception as e:
		print e

	output['country']=out_countries
	if len(check_countries)>0:
		_index=check_countries[0]
	else:
		_index=''
	return output,_index

def getIntentType(key):
	try:
	

		tweets_db = MongoClient(mongo_url,27017)["GnipDataFinal"]["tweets"]
		
		intent_data = tweets_db.aggregate([{"$match":{"alertid":{"$in":[key]},"tweet.textType":{"$exists":1}}},{"$group":{"_id":"$tweet.textType","count":{"$sum":1}}}])
		total_intent = tweets_db.find({"alertid":{"$in":[key]},"tweet.textType":{"$exists":1}}).count()
		intent_output={}
		for intent in intent_data:
			value = round(intent['count']*100/float(total_intent),2)
			if intent['_id']=='' or intent['_id']=='NA':
				intent['_id']= 'others'
			intent_output[intent['_id']] = value
		# print intent_output
		return intent_output

		
	except:
		print traceback.format_exc()
		return {}




def GetEmotionData(key):
	try:
		defaultEmotion = ["bored","excited", "happy", "sad", "angry", "disgusted", "funny", "fear", "indifferent","NA",None]
		tweets_db = MongoClient(mongo_url,27017)["GnipDataFinal"]["tweets"]
		emotionType = tweets_db.aggregate([{"$match":{"alertid":{"$in":[key]},"tweet.emotionType":{"$ne":'NA'}}},{"$group":{"_id":"$tweet.emotionType","count":{"$sum":1}}}])
		countEmotion = [val for val in emotionType]
		totalSum = 0
		emoDict = {}
		if len(countEmotion) > 0:
			for r in range(len(countEmotion)):
				totalSum = totalSum + countEmotion[r]["count"]
			for percentag in range(len(countEmotion)):
				emoDict[countEmotion[percentag]["_id"]] = round((float(countEmotion[percentag]["count"])/totalSum)*100,2)
			return {'funny': float(emoDict.get('funny',0)), 'indifferent':float(emoDict.get('indifferent',0)), 'angry': float(emoDict.get('angry',0)), 'sad': float(emoDict.get('sad',0)), 'fear': float(emoDict.get('fear',0)), 'excited': float(emoDict.get('excited',0)), 'happy': float(emoDict.get('happy',0))}
	except:
		print traceback.format_exc()
		return {'funny': 0.0, 'indifferent': 0.0,'angry': 0.0, 'sad': 0.0, 'fear': 0.0, 'bored': 0.0, 'excited': 0.0, 'happy': 0.0}


# @profile
def analyze_data_thirty_days(alert_info):
	
	try:
		# print alert_info
		tw_status=alert_info['tw_status']
		key=alert_info['alert_id']
		modified_time=alert_info['modified_time']
		status=alert_info['status']
		data_analysis=MongoClient(mongo_url,27017)["dev_twitter"]["analyticsData"]
		tweets_db=MongoClient(mongo_url,27017)["GnipDataFinal"]["tweets"]
		
		current_time=datetime.datetime.now()
		diff_time=0
		if modified_time is not None:
			diff_time=(current_time-modified_time).total_seconds()/3600
		if (status=='active' and tw_status=='active') or (diff_time<0.25):
			print key

			alert_data=tweets_db.find({"alertid":{"$in":[key]}})
			tweets_data= [x for x in alert_data]
			tweets_analysis=pd.DataFrame()
			tweets_analysis['tweet_type']=map(lambda tweet:tweet.get('tweet').get('tweet_status',None),tweets_data)
			tweets_analysis['tweet_lang']=map(lambda tweet:tweet.get('tweet').get('language',None),tweets_data)
			tweets_analysis['sentiment']=map(lambda tweet:tweet.get('sentiment',None),tweets_data)

			intent_data =getIntentType(key)
			tweetEmotion = GetEmotionData(key)
			countries_data=get_countries(key)
			countries=countries_data[0]
			country=countries_data[1].encode('ascii','ignore')

			location=get_location(key,country)
			influencers=overall_influencersForHashtag_v2(key)
			en_influencers=eng_influencersForHashtag_v2(key)
			keywords_v3=keywords_analysis(key)
			keywords_excl_retweets = keywords_excluded_retweets(key)
			hashtags_v2=hashtagsForTweets_v2(key)
			hashtags_excl_retweets = hashtags_excluded_retweets(key)
			sentiment_data=get_stats_data(key,tweets_analysis)
			positive_stats=sentiment_data[0]
			negative_stats=sentiment_data[1]
			neutral_stats=sentiment_data[2]
			undefined_stats=sentiment_data[3]
			counter_data=get_counter_data(key,tweets_analysis)
			tweets_count=counter_data[0]
			retweets_count=counter_data[1]
			undefined_tweets=counter_data[2]
			undefined_retweets=counter_data[3]


			
			output={"tagid":key,"tweet":{"keywords_excluded_retweets":keywords_excl_retweets,"hashtags_excluded_retweets":hashtags_excl_retweets,"textType":intent_data,"tweetEmotion":tweetEmotion,"keywords_v3":keywords_v3,"influencers_v2":en_influencers,"hashtags_v2":hashtags_v2,"location":location,"countries":countries,"keyword_v2":keywords_v3,"stats":{"last_updated":str(datetime.datetime.now()),"positive":positive_stats,"negative":negative_stats,"neutral":neutral_stats,"undefined_stats":undefined_stats},"influencers":influencers,"en_influencers":en_influencers,"keywords":keywords_v3,"hashtags":hashtags_v2,"positive":[],"negative":[],"popular":[],"counter":{"original":tweets_count,"retweets":retweets_count,"undefined_tweets":undefined_tweets,"undefined_retweets":undefined_retweets}}}


			final_output={"bucket":'analysis',"tracking_keyword":key,"tweet":output['tweet'],"alertid":key}

			 
			posts=data_analysis.find_one({"alertid":key,"tracking_keyword":key})
			if posts is not None:
				data_analysis.update({'_id': ObjectId(str(posts['_id']))},{'$set':final_output})
			else:
				data_analysis.insert(final_output)

	except Exception as e:
		print e




if __name__ == '__main__':

	print "STARTED EVERY HOUR JOB"
	sql_data=[]
	sql_output=get_sql_data(sql_data)
	print datetime.datetime.now()
	start=0
	end=start+10
	# hashtagsForTweets_v2(1591)
	# analyze_data_thirty_days({"tw_status":'active',"alert_id":1563,"modified_time":datetime.datetime(2017,01,10,0,0),"status":'active'})
	while start<=len(sql_output):
		print start,end
		parmap.map(analyze_data_thirty_days,sql_output[start:end])
		if start>=len(sql_output):
			break
		start=end
		end=start+10
		if end>len(sql_output):
			end=len(sql_output)
	print datetime.datetime.now()
	print "COMPLETED EVERY HOUR JOB"