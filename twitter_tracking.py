import os
import json
import time
import calendar
import datetime
import ast
import hashlib
from memsql.common import database
import MySQLdb
import ast
import operator
import mandrill
import requests
from pymongo import MongoClient
import datetime

HOST     = "memsql.paralleldots.com"
USER     = "ankit"
PASSWORD = "mongodude123"

DATABASE = "socialmedia"

mongo_dev_url = os.environ['MONGOURL']
gnip_db = MongoClient(mongo_dev_url,27017)["GnipDataFinal"]["tweets"]

def get_connection(db=DATABASE):
	return database.connect(host=HOST,user=USER, password=PASSWORD, database=db)

def tracktwitter():
	with get_connection(db=DATABASE) as conn:
		print "============================="
		userid_list = conn.query('SELECT DISTINCT uid FROM twittertrack')
		print "userid_list",userid_list
		for user in userid_list:
			uid = user['uid']
			print "uid",uid
			user_info = conn.query('SELECT * FROM users where uid=%s',uid)
			print "user_info",user_info
			if len(user_info) != 0:
				quota = user_info[0]['quota']
				user_email = user_info[0]['email']
				used_quota = user_info[0]['used_quota']
				individual_quota = user_info[0]['used_individual_quota']
				print "quota,user_email,used_quota,individual_quota",quota,user_email,used_quota,individual_quota
				if individual_quota is not None:
					individual_quota = ast.literal_eval(individual_quota)
				else:
					individual_quota = {}
				if used_quota >= quota:
					print "quota exceeded"
					continue
				else:
					myalerts = conn.query("SELECT * from myalerts where uid=%s",uid)
					print "len of myalerts",len(myalerts)
					if len(myalerts) != 0:
						alert_count = 0
						used_count = 0
						for alert in myalerts:
							#print "alert",alert
							alert_count += 1
							alert_creation_time = alert['created_at']
							alertid = alert['id']
							print "alertid",alertid
							tagname = conn.query('SELECT tagname from twittertrack where uid=%s AND alertid=%s',uid,alertid)
							print "length of tagname",len(tagname)
							if len(tagname) != 0:
								tagname = tagname[0]['tagname']
								#tagname = tagname.encode("ascii","ignore")
								print "tagname",tagname
								count = gnip_db.find({"$and":[{"tagname":tagname},{"tweet.timestamp":{"$gte":alert_creation_time}},{"subscribers":{"$in":[uid]}}]}).count()
								print "count",count
								#individual_quota[tagname] = float((count*1.0)/(quota*1.0)*100)
								individual_quota[tagname] = count
								used_count+=count
								print "used_count",used_count
						conn.query('UPDATE users SET used_quota=%s where uid=%s',used_count,uid)	
						conn.query('UPDATE users SET used_individual_quota=%s where uid=%s',str(individual_quota),uid)
						
						get_count_in_percentage = int(((used_quota*1.0)/(quota*1.0))*100.0)

						inactive_count = 0
						
						if get_count_in_percentage >= 50 and get_count_in_percentage <= 60:
							print "used 50percent of data"
						elif get_count_in_percentage >= 61 and get_count_in_percentage <= 80:
							print "used "+ str(used_quota*1.0/100000.0*100.0)+ "of stream"
						elif get_count_in_percentage >= 81 and get_count_in_percentage < 100:
							print "used "+ str(used_quota*1.0/100000.0*100.0)+ "of stream"
						elif get_count_in_percentage >=100:
							alertid_list = conn.query('SELECT alertid FROM twittertrack where uid=%s',uid)
							
							for alerts in alertid_list:
								data = {}
								alertid = alerts['alertid']
								myalerts_db = conn.query('SELECT * FROM myalerts where id=%s',int(alertid))
								status = myalerts_db[0]['status']
								
								if status == 'inactive':
									print "already inactive"
								else:
									print "active"
									inactive_count+=1
									data['status']  =  'inactive'
									data['user_id'] =  uid
										
									if myalerts_db[0].get('keyword') is None:
										data['main_keyword']  =  ""
									else:
										data['main_keyword']  =  str(myalerts_db[0].get('keyword'))
									
									if myalerts_db[0].get('optional_kw') is None:
										data['optional_kw']   =  ""
									else:
										data['optional_kw']   =  str(myalerts_db[0].get('optional_kw'))
									
									if myalerts_db[0].get('fburl') is None:
										data['fburl']         =  ""
									else:
										data['fburl']         =  eval(myalerts_db[0].get('fburl'))
									
									if myalerts_db[0].get('alertname') is None:
										data['alertname']     =  ""
									else:
										data['alertname']     =  str(myalerts_db[0].get('alertname'))
									
									if myalerts_db[0].get('hashtags') is None:
										data['hashtags']      =  ""
									else:
										data['hashtags']      =  str(myalerts_db[0].get('hashtags'))
									
									if myalerts_db[0].get('twitter_track_type') is None:
										data['twitter_track_type']      =  ""
									else:
										data['twitter_track_type']      =  str(myalerts_db[0].get('twitter_track_type'))

									if myalerts_db[0].get('publisher') is None:
										data['publisher']     =  ""
									else:
										data['publisher']     =  str(myalerts_db[0].get('publisher'))
									
									data['aid']               =  alertid
									r = requests.post('http://karnadev.paralleldots.com/karna/apis/alerts/update', data = json.dumps(data))
									print "response for update",r
							status = "inactive"
							stop_type = "admin"
							now = datetime.datetime.now()
							conn.query('UPDATE users SET inactive_time=%s where uid=%s',str(now),uid) 
							conn.query('UPDATE users SET status=%s where uid=%s',status,uid)
							conn.query('UPDATE users SET twitter_tracking_stop_type=%s where uid=%s',stop_type,uid)
							print "sendmail"
	conn.close()        		         
tracktwitter()
